#!/usr/bin/env python

from src import bf, kf, ms

def main():

    choose = None

    print("1. Barnsley Fern")
    print("2. Koch Fractal")
    print("3. Mandelbrot Set")
    print("q: Quit")

    while choose != "q":

        choose = str(input("Select the fractal you wish to generate: "))
    
        if choose == "1":
            bf.main()
        elif choose == "2":
            kf.main()
        elif choose == "3":
            ms.main()
        elif choose == "q":
            break

main()